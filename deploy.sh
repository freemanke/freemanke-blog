rm -rf ./site/*
python -m mkdocs build --clean
ssh root@freemanke.com << EOF
rm -rf /root/freemanke-blog/
mkdir /root/freemanke-blog
EOF
scp -r ./site/* root@freemanke.com:~/freemanke-blog