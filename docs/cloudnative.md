# 云原生现状

## 容器原理

- Linux Namespace 解决了文件系统、资源隔离问题

- Control Groups 解决物理资源的隔离和配额使用问题

- UnionFS 解决了文件系统读写问题

## 云原生

技术的变革，一定是思想先行，云原生是一种构建和运行应用程序的方法，是一套技术体系和方法论。云原生是一个组合词，Cloud+Native。Cloud表 示应用程序位于云中，而不是传统的数据中心；Native 表示应用程序从设计之初即考虑到云的环境，原生为云而设计，在云上以最佳姿势运行，充分利用和发挥云平台的弹性和分布式优势。
![img](images/v2-f51c7a7bec2978533f5e6aaffe4d2faa_720w.jpg)

## 构成要素

1. 微服务化

2. 容器化

3. DEVOPS

4. 持续交付

## 技术的趋势和影响

软件设计有两个关键目标：**高内聚、低耦合**，围绕这 2 个核心目标，又提出了单一职责、开闭原则、里氏替换、依赖导致、接口隔离、最少知识等设计原则。软件工程一直都在为这两个目标而努力奋斗，以求把软件编写得更加清晰、更加健壮、更加易于扩展和维护。

## DEVOPS 核心概念

构建全功能团队，测试驱动开发，快速迭代，小步快走，严格的代码审查，高效高覆盖率的自动化测试，持续集成，持续部署，持续交付，持续反馈，持续沟通，持续更新。

![img](images/devops-tools.png)

![devops-infinity-1-1 DevOps ](images/devops-infinity-1-1.png)

## K8S 技术原理

相比 Docker 解决了什么问题？

- 建立容器之间的通信子网如隧道、路由等，解决容器的跨节点通信问题。
- 内置 DNS 服务和 kube-proxy 用于服务发现及负载均衡
- 资源管理，包括容器使用的资源限制，以及将容器调度到对用的集群节点上
- Docker 也有 Volume 的概念，但是相对简单，Kubernetes 对 Volume 则有着清晰定义和广泛的支持。
- 对容器运行状态的监控

![Deployment evolution](images/container_evolution.svg)

### 容器组件

![Kubernetes](images/kubernetes-control-plane.png)

## 云原生路线图
![CNCF_TrailMap_latest](images/CNCF_TrailMap_latest.png)